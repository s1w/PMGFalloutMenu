//
//  ViewController.swift
//  PMGFalloutMenu
//
//  Created by Yevhenii Veretennikov on 3/6/17.
//  Copyright © 2017 ensies. All rights reserved.
//

import UIKit

class ViewController: UIViewController, PMGFalloutMenuDelegate {

    var fallMenuHeight = NSLayoutConstraint()
    
    lazy var fallMenu: PMGFalloutMenu = {
        let view = PMGFalloutMenu()
        view.menuTitle = "ПОДПИСКИ"
        view.tabSectionsNames = ["ПОДПИСКИ", "ПОДПИСЧИКИ", "ПОИСК"]
        view.delegate = self
        view.arrowImage = #imageLiteral(resourceName: "downArrow")
        view.titleColor = UIColor.black
        view.cellHeight = 120
        view.backgroundColor = .clear
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
        view.addSubview(fallMenu)
        
        let fallMenuTop = NSLayoutConstraint(item: fallMenu, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 20)
        let fallMenuLeft = NSLayoutConstraint(item: fallMenu, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0)
        let fallMenuRight = NSLayoutConstraint(item: fallMenu, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0)
        fallMenuHeight = NSLayoutConstraint(item: fallMenu, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: fallMenu.cellHeight)
        
        self.view.addConstraints([fallMenuTop, fallMenuLeft, fallMenuRight, fallMenuHeight])
    }
    
    // MARK: - Fallout menu delegate
    
    func falloutMenu(withSize: CGFloat) {
        self.fallMenuHeight.constant = self.fallMenu.isMenuOpened ? self.fallMenu.cellHeight : withSize
    }
    
    func falloutMenu(_ falloutMenu: PMGFalloutMenu, didSelectItemAt index: Int) {
        print(index)
    }
    
}



























