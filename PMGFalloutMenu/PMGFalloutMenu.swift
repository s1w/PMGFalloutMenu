//
//  PMGFalloutMenu.swift
//  PMGFalloutMenu
//
//  Created by Yevhenii Veretennikov on 3/7/17.
//  Copyright © 2017 ensies. All rights reserved.
//

import UIKit

protocol PMGFalloutMenuDelegate {
    func falloutMenu(withSize: CGFloat)
    func falloutMenu(_ falloutMenu: PMGFalloutMenu, didSelectItemAt index: Int)
}

class PMGFalloutMenu: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    open var arrowImage = UIImage() {
        didSet {
            self.arrowImageView.image = arrowImage.withRenderingMode(.alwaysTemplate)
        }
    }
    
    open var menuTitle = String() {
        didSet {
            self.headerLabel.text = menuTitle
        }
    }
    open var tabSectionsNames = [String]() {
        didSet {
            self.fullExpandedHeight = CGFloat(tabSectionsNames.count) * singleCellHeight
            self.collectionView.reloadData()
        }
    }
    open var titleColor = UIColor.white {
        didSet {
            self.headerLabel.textColor = titleColor
            self.arrowImageView.tintColor = titleColor
        }
    }
    
    open var cellHeight: CGFloat = 10 {
        didSet {
            self.singleCellHeight = cellHeight
            self.fullExpandedHeight = CGFloat(tabSectionsNames.count) * singleCellHeight
        }
    }
    
    open var isMenuOpened = false
    open var delegate: PMGFalloutMenuDelegate?
    
    private var collectionViewHeight = NSLayoutConstraint()
    private let cellId = "PMGFalloutMenuCellId"
    private var singleCellHeight = CGFloat(55)
    private var fullExpandedHeight = CGFloat()
    
    // MARK: - Setup all view elements
    
    private let rootView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var arrowImageView: UIImageView = {
        let view = UIImageView()
        view.tintColor = .white
        view.contentMode = .scaleAspectFit
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(falloutMenu(withSize:))))
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var headerLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .center
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(falloutMenu(withSize:))))
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        return cv
    }()
    
    // MARK: -
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.isUserInteractionEnabled = true
        self.backgroundColor = .clear
        self.headerLabel.textColor = self.titleColor
        self.collectionView.register(PMGFalloutMenuTabCell.self, forCellWithReuseIdentifier: cellId)
        self.setupConstraints()
        self.layoutIfNeeded()
    }
    
    private func setupConstraints() {
        self.addSubview(rootView)
        self.addSubview(headerLabel)
        self.addSubview(arrowImageView)
        self.addSubview(collectionView)
        
        self.setupRootView()
        self.setupHeaderLabel()
        self.setupArrowImageView()
        self.cetupCollectionView()
    }
    
    // MARK: - Setup constraints
    
    private func setupRootView() {
        let topAnchor = NSLayoutConstraint(item: rootView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let leftAnchor = NSLayoutConstraint(item: rootView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0)
        let rightAnchor = NSLayoutConstraint(item: rootView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0)
        let heightAnchor = NSLayoutConstraint(item: rootView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 44)
        
        self.addConstraints([topAnchor, leftAnchor, rightAnchor, heightAnchor])
    }
    
    private func setupHeaderLabel() {
        let topAnchor = NSLayoutConstraint(item: headerLabel, attribute: .top, relatedBy: .equal, toItem: rootView, attribute: .top, multiplier: 1, constant: 0)
        let bottomAnchor = NSLayoutConstraint(item: headerLabel, attribute: .bottom, relatedBy: .equal, toItem: rootView, attribute: .bottom, multiplier: 1, constant: 0)
        let centerXAnchor = NSLayoutConstraint(item: headerLabel, attribute: .centerX, relatedBy: .equal, toItem: rootView, attribute: .centerX, multiplier: 1, constant: 0)
        
        self.addConstraints([topAnchor, bottomAnchor, centerXAnchor])
    }
    
    private func setupArrowImageView() {
        let leftAnchor = NSLayoutConstraint(item: arrowImageView, attribute: .left, relatedBy: .equal, toItem: headerLabel, attribute: .right, multiplier: 1, constant: 8)
        let widthAnchor = NSLayoutConstraint(item: arrowImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 10)
        let heightAnchor = NSLayoutConstraint(item: arrowImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 10)
        let centerYAnchor = NSLayoutConstraint(item: arrowImageView, attribute: .centerY, relatedBy: .equal, toItem: rootView, attribute: .centerY, multiplier: 1, constant: 1)
        
        self.addConstraints([leftAnchor, widthAnchor, heightAnchor, centerYAnchor])
    }
    
    private func cetupCollectionView() {
        let topAnchor = NSLayoutConstraint(item: collectionView, attribute: .top, relatedBy: .equal, toItem: rootView, attribute: .bottom, multiplier: 1, constant: 0)
        let leftAnchor = NSLayoutConstraint(item: collectionView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0)
        let rightAnchor = NSLayoutConstraint(item: collectionView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0)
        let widthAnchor = NSLayoutConstraint(item: collectionView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.width)
        collectionViewHeight = NSLayoutConstraint(item: collectionView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        
        self.addConstraints([topAnchor, leftAnchor, rightAnchor, collectionViewHeight, widthAnchor])
    }
    
    // MARK: - Self delegate
    
    func falloutMenu(withSize: CGFloat) {
        
        self.collectionViewHeight.constant = self.isMenuOpened ? 0 : self.fullExpandedHeight
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.layoutIfNeeded()
        }, completion: nil)
        
        self.delegate?.falloutMenu(withSize: collectionViewHeight.constant+cellHeight)
        
        UIView.animate(withDuration: 0.2) {
            self.arrowImageView.transform = self.isMenuOpened ? CGAffineTransform(rotationAngle: 0) : CGAffineTransform(rotationAngle: CGFloat.pi)
        }
        
        isMenuOpened = !isMenuOpened
        UIView.setAnimationsEnabled(true)
    }
    
    func falloutMenu(_ falloutMenu: PMGFalloutMenu, didSelectItemAt index: Int) {
        self.delegate?.falloutMenu(falloutMenu, didSelectItemAt: index)
    }
    
    // MARK: - Collection view delegate methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabSectionsNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PMGFalloutMenuTabCell
        cell.tabLabelName = tabSectionsNames[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: singleCellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.falloutMenu(withSize: collectionViewHeight.constant+cellHeight)
        self.headerLabel.text = tabSectionsNames[indexPath.item]
        falloutMenu(self, didSelectItemAt: indexPath.item)
    }
    
}


