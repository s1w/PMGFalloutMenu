//
//  PMGFalloutMenuTabCell.swift
//  PMGFalloutMenu
//
//  Created by Yevhenii Veretennikov on 3/7/17.
//  Copyright © 2017 ensies. All rights reserved.
//

import UIKit

class PMGFalloutMenuTabCell: UICollectionViewCell {
    
    var tabLabelName = String() {
        didSet {
            self.tabLabel.text = tabLabelName
        }
    }
    
    // MARK: -
    
    private let separator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let tabLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .center
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    // MARK: -
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        UIView.setAnimationsEnabled(false)
        
        self.backgroundColor = UIColor.white
        self.translatesAutoresizingMaskIntoConstraints = false
        
        setupConstraionts()
    }
    
    private func setupConstraionts() {
        self.addSubview(separator)
        self.addSubview(tabLabel)
        
        setupSeparator()
        setupTabLabel()
    }
    
    private func setupSeparator() {
        let botAnchor = NSLayoutConstraint(item: separator, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        let leftAnchor = NSLayoutConstraint(item: separator, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0)
        let rightAnchor = NSLayoutConstraint(item: separator, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0)
        let heightAnchor = NSLayoutConstraint(item: separator, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 1)
        
        self.addConstraints([botAnchor, heightAnchor, leftAnchor, rightAnchor])
    }
    
    private func setupTabLabel() {
        let centerX = NSLayoutConstraint(item: tabLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        let centerY = NSLayoutConstraint(item: tabLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        
        self.addConstraints([centerX, centerY])
    }
    
}
